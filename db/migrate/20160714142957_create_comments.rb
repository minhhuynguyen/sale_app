class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.text :user_name
      t.text :content
      t.integer :source_id
      t.string :source_type

      t.timestamps null: false
    end
    
  end
end
