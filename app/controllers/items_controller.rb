class ItemsController < ApplicationController
  
  def edit
    @item = Item.find(params[:id])
  end

  def new
  	@item = Item.new
  end

  def show
    @item     = Item.find(params[:id])
    #@comment  = @item.comments.build
    #@comment = Comment.find_by(:id => params[:comment_id])

    @replies = Comment.where ({source_type: "Comment"})    
    @comment_id = params[:comment_id]


    #comment_id = @comment.id
    # debugger
    #@reply    = @comment.replies.build
    respond_to do |format|
      format.html
      format.json
      #format.js { render :show, type: params[:type] }
      format.js
    end


  end

  def create
    @item = Item.new(item_params)
    if @item.save
      # Handle a successful save.
      redirect_to @item
    else
      render 'new'
    end
  end


  def update
    @item = Item.find(params[:id]) 
    respond_to do |format|
      if @item.update(item_params)
        format.html { redirect_to @item, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @item }
      else
        format.html { render :edit }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @item = Item.find(params[:id])
    @item.destroy
    respond_to do |format|
      format.html { redirect_to root_path, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def item_params
      params.require(:item).permit(:image, :name, :description)
    end

    def comment_params
      params.require(:comment).permit(:source_id, :source_type => 'Item')
    end
end
