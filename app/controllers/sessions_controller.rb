class SessionsController < ApplicationController
  def new
  end

  def create
  	user = User.find_by(user_name: params[:session][:user_name])
  	if user && user.authenticate(params[:session][:password])
  		log_in user
      redirect_to root_path

  	else
      flash.now[:danger] = 'Invalid username/password'  
  		render 'new'
  	end
  	#render 'new'
  end

  def destroy
    log_out
    redirect_to root_path
  end
end
