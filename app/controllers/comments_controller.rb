class CommentsController < ApplicationController
  
  def show
    item = Item.find(params[:item_id])
    

    respond_to do |format|
      format.html { }
      format.json { }
    end    

  end



  def create
    item = Item.find(params[:item_id])
    comment = Comment.new(comment_params)

    if comment.save
      respond_to do |format|
        format.html { }
        format.json { }
      end

      #UserMailer.notify_comment(item, comment).deliver
    else
      #flash[:error] = comment.errors.full_messages.join("\n")
      flash[:error] = comment.errors.full_messages
    end

    redirect_to item
  end

  def destroy
    item = Item.find(params[:item_id])
    comment = Comment.find(params[:id])
    comment.destroy
    respond_to do |format|
      format.html { redirect_to item, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def comment_params
      params.require(:comment).permit(:user_name, :content, :source_id, :source_type)
    end
end
