class ApplicationMailer < ActionMailer::Base
  default from: "ng.huyminh91@gmail.com"
  layout 'mailer'
end
