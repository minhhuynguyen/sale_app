class UserMailer < ApplicationMailer
  #default from: 'ng.huyminh91@gmail.com'
  default from: 'develop_rails_4fun@zoho.com'
  def notify_comment(item, comment)
    @item = item
    @comment = comment
    #mail(to: 'volephuongtrinh1991@gmail.com', subject: 'Someone has commented to your item')
    subj_str = "#{@comment.user_name} has commented to your item"
    mail(to: 'ng.huyminh91@gmail.com', subject: subj_str)
  end
end
