class Item < ActiveRecord::Base
  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
  validates_with AttachmentSizeValidator, attributes: :image, less_than: 1.megabytes

  after_validation :clean_paperclip_errors

  def clean_paperclip_errors
    errors.delete(:image)
  end

  def comments
  	Comment.where({ source_id: self.id, source_type: 'Item' })
  end
end
