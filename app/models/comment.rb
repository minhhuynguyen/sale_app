class Comment < ActiveRecord::Base


  # validates :user_name, presence: true
  # validates :content, presence: true, length: {in: 30..500}

  #validate :comment_count_limit, :on => :create
#
#  #def comment_count_limit
#  #  limit_value = 10
#  #  if self.item.comments(:reload).count >= limit_value 
#  #    errors.add(:base, "Exceed the limitation number of comments. Only #{limit_value} comments are allowed")
#  #  end
  #end

  def replies
    Comment.where({ source_id: self.id, source_type: 'Comment' })
  end
end