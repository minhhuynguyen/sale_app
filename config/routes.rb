Rails.application.routes.draw do


  root              'sale_pages#home'
  get '/help'     , to: 'sale_pages#help'
  get '/contact'  , to: 'sale_pages#contact'
  get '/new'      , to: 'items#new'
  
  get    '/login'    , to: 'sessions#new'
  post   '/login'    , to: 'sessions#create'
  delete '/logout'   , to: 'sessions#destroy'

  resources :items do 
  	resources :comments
  end

  resources :users
  #post 'items/:item_id/comments', to: 'comments#create'
end
